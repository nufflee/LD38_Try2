package com.vitogaming.ld38.util;

public class Logger {
	public static void debug(String message){
		System.out.println("DEBUG: " + message);
	}

	public static void info(String message){
		System.out.println("INFO: " + message);
	}

	public static void warn(String message){
		System.out.println("WARNING: " + message);
	}

	public static void error(String message){
		System.out.println("ERROR: " + message);
	}

	public static void error(Exception e){
		System.out.println("ERROR: " + e.getMessage());
	}
}
