package com.vitogaming.ld38.game;

import asciiPanel.AsciiFont;
import com.vitogaming.ld38.commands.CommandManager;
import com.vitogaming.ld38.util.Logger;

import javax.swing.*;

public class Game extends JFrame{
	private GameTerminal terminal;
	private FrameCounter frameCounter;
	private CommandManager commandManager;

	public Game() {
		super();
		terminal = new GameTerminal(80, 30, AsciiFont.CP437_9x16, this);
		frameCounter = new FrameCounter();
		commandManager = new CommandManager(this);


		add(terminal.getAsciiTerminal());
		pack();

		terminal.write("Hello! Welcome to the HAKAAR 1038!", 0, 0);
		terminal.write("To learn more about the HAKAAR 1038 type /about, to learn more about your mission type /mission.", 0, 1);

		addKeyListener(terminal);

		new Thread(() -> {
			while(true){
				frameCounter.update();
				terminal.getAsciiTerminal().repaint();

				Logger.info(terminal.currentLine);

				try {
					Thread.sleep(1000 / 7);
				} catch (InterruptedException e) {
					Logger.error("Update is interrupted!");
					Thread.currentThread().interrupt();
				}
			}
		}).start();
	}


	@Override
	public void repaint(){
		terminal.getAsciiTerminal().clear();

		super.repaint();
	}

	public FrameCounter getFrameCounter(){
		return frameCounter;
	}

	public GameTerminal getTerminal() {
		return terminal;
	}

	public CommandManager getCommandManager() {
		return commandManager;
	}
}
