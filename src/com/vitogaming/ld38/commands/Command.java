package com.vitogaming.ld38.commands;

import com.vitogaming.ld38.util.Initiable;

public class Command {
	private String name;
	private String description;
	private String responseText;
	private Initiable response;

	Command(String name, String description, String responseText, Initiable response){
		this.name = name;
		this.description = description;
		this.responseText = responseText;
		this.response = response;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	public String getResponseText() {
		return responseText;
	}

	public Initiable getResponse() {
		return response;
	}
}
