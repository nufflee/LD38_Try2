package com.vitogaming.ld38.game;

public class FrameCounter {
	private int totalFrames;

	public void update(){
		totalFrames++;
	}

	public int getTotalFrames(){
		return totalFrames;
	}
}
