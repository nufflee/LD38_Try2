package com.vitogaming.ld38.util;

public interface Initiable {
	void init(Object ... args);
	void run();
}
