package com.vitogaming.ld38.commands;


import com.vitogaming.ld38.game.Game;
import com.vitogaming.ld38.util.Initiable;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class CommandManager {
	private static List<Command> commands;
	private Game game;

	public CommandManager(Game game){
		this.game = game;

		commands = new ArrayList<>();

		commands.add(new Command("about", "Tells you all about the HAKAAR 1038", "HAKAAR 1038 is the newest hacking tool available on " +
				"the Market! It is 500% faster than it's predecessors! Now you can hack 10 servers at once! Isn't that cool?!",  new Initiable() {
			int x;
			int y;

			@Override
			public void init(Object... args) {
				x = (int)args[0];
				y = (int)args[1];
			}

			@Override
			public void run() {
				game.getTerminal().write("HAKAAR 1038 is the newest hacking tool available on the Market! It is 500% faster than it's predecessors! " +
						"Now you can hack 10 servers at once! Isn't that cool?!", x, y, Color.YELLOW);
			}
		}));

		commands.add(new Command("mission", "Tells you all about your mission", null, new Initiable() {
			int x;
			int y;

			@Override
			public void init(Object... args) {
				x = (int)args[0];
				y = (int)args[1];
			}

			@Override
			public void run() {

			}
		}));
	}

	public int processCommand(String commandString, int x, int y){
		for (int i = 0; i < commandString.length(); i++){
			if(("/" + commands.get(i).getName()).equalsIgnoreCase(commandString)){
				Command command = commands.get(i);
				command.getResponse().init(x, y);
				command.getResponse().run();
				return command.getResponseText().length() > game.getTerminal().getAsciiTerminal().getWidthInCharacters() ? 2 : 1;
			}
		}

		return 0;
	}

	public static List<Command> getCommands() {
		return commands;
	}
}
