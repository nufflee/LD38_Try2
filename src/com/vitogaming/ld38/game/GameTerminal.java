package com.vitogaming.ld38.game;

import asciiPanel.AsciiFont;
import asciiPanel.AsciiPanel;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameTerminal implements KeyListener {
	private AsciiPanel terminal;
	private Game game;
	private int currentX = 0;
	private int currentY = 2;
	public String currentLine = "";

	public GameTerminal(int width, int height, AsciiFont font, Game game) {
		this.game = game;

		terminal = new AsciiPanel(width, height, font);
	}

	public void write(String text, int x, int y){
		String secondPart;

		if(text.length() > terminal.getWidthInCharacters()){
			secondPart = text.substring(0, terminal.getWidthInCharacters() - 1);
			text = text.substring(terminal.getWidthInCharacters() - 1, text.length());
		}

		terminal.write(text, x, y);
		terminal.write(text, 0, y + 1);
	}

	public void write(String text, int x, int y, Color color){
		String secondPart = "";

		if(text.length() > terminal.getWidthInCharacters()){
			secondPart = text.substring(terminal.getWidthInCharacters(), text.length());
			text = text.substring(0, terminal.getWidthInCharacters());
		}

		terminal.write(text, x, y, color);
		terminal.write(secondPart, 0, y + 1, color);
	}

	@Override
	public void keyTyped(KeyEvent e) {
		if (currentX > terminal.getWidthInCharacters()) {
			currentX = 0;
			currentY++;
			currentLine = "";
			return;
		}

		if(currentX < 0){
			currentX = 0;
			return;
		}

		if (e.getKeyChar() == '\b') {
			terminal.write(" ", currentX - 1, currentY);
			currentX--;
			currentLine = currentLine.substring(0, currentLine.length() - 1);
			return;
		}

		if(e.getKeyChar() == '\n'){
			currentX = 0;
			currentY++;

			currentY += game.getCommandManager().processCommand(currentLine, currentX, currentY);

			currentLine = "";
			return;
		}

		terminal.write(e.getKeyChar(), currentX++, currentY);
		currentLine += e.getKeyChar();
	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	public AsciiPanel getAsciiTerminal() {
		return terminal;
	}
}
