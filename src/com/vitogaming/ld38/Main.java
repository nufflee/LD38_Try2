package com.vitogaming.ld38;

import com.vitogaming.ld38.game.Game;

import javax.swing.*;

public class Main {
	public static Game game;

	public static void main(String[] args){
		game = new Game();
		game.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		game.setVisible(true);
		game.setTitle("LD 38");
	}
}
